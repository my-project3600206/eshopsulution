﻿using eShopSolution.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShopSolution.Data.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ShipName).IsRequired().IsUnicode().HasMaxLength(50);
            builder.Property(x => x.ShipAddress).IsRequired().IsUnicode().HasMaxLength(50);
            builder.Property(x => x.ShipEmail).IsRequired().IsUnicode().HasMaxLength(50);
        }
    }
}
