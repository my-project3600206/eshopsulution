﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShopSolution.Data.Entities
{
    public class OrderDetail
    {
        public int OrderId { set; get; }
        public int ProductId { set; get; }
        public int Quantity { set; get; }
        public decimal Price { set; get; }

        // order and orderDetail is one to many relationShip
        public Order Order { set; get; } // nên thêm trường này
        public Product Product { set; get; }
        
    }
}
